set term png medium
set output "output.png"
set key top left
set autoscale
set title "Experiment heap_fill_huge"
set xlabel "Time (seconds)"
set ylabel "Bytes"
plot "24461_heap.data" using 1:2 with lines lw 2 title "heap", "24461_stack.data" using 1:2 with lines lw 2 title "stack", "24461_physical.data" using 1:2 with lines lw 2 title "os"

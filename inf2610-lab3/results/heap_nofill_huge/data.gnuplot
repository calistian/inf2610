set term png medium
set output "output.png"
set key top left
set autoscale
set title "Experiment heap_nofill_huge"
set xlabel "Time (seconds)"
set ylabel "Bytes"
plot "24589_heap.data" using 1:2 with lines lw 2 title "heap", "24589_stack.data" using 1:2 with lines lw 2 title "stack", "24589_physical.data" using 1:2 with lines lw 2 title "os"

set term png medium
set output "output.png"
set key top left
set autoscale
set title "Experiment stack_fill_huge"
set xlabel "Time (seconds)"
set ylabel "Bytes"
plot "24931_heap.data" using 1:2 with lines lw 2 title "heap", "24931_stack.data" using 1:2 with lines lw 2 title "stack", "24931_physical.data" using 1:2 with lines lw 2 title "os"

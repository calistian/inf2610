/*
 * baz.c
 *
 *  Created on: 2013-08-15
 *      Author: Francis Giraldeau <francis.giraldeau@gmail.com>
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include "whoami.h"

int main(int argc, char **argv) {
        // On execute ce programme
	increment_rank();
	whoami("baz");

	// Decrementation de n
	int n = atoi(argv[1]) - 1;
	char * x;
	asprintf(&x, "%d", n);

	// On poursuit la chaine d'executions
	if(n>0)
		execlp("foo", "foo", x, NULL);
}

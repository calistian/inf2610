/*
 * bar.c
 *
 *  Created on: 2013-08-15
 *      Author: Francis Giraldeau <francis.giraldeau@gmail.com>
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include "whoami.h"

int main(int argc, char **argv) {
        // On execute ce programme
	increment_rank();
	whoami("bar");

	// On poursuit la chaine d'executions
        execlp("baz", "baz", argv[1], NULL);
}

/*
 * semrelay.c
 *
 *  Created on: 2013-08-19
 *      Author: Francis Giraldeau <francis.giraldeau@gmail.com>
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <semaphore.h>

#include "semrelay.h"
#include "statistics.h"
#include "multilock.h"
#include "utils.h"

void *semrelay_worker(void *ptr) {
    unsigned long i, j;
    struct experiment *exp = ptr;

    for (i = 0; i < exp->outer; i++) {
        // Attendre notre tour
        sem_wait(exp->lock + (exp->rank)*sizeof(sem_t));
        for (j = 0; j < exp->inner; j++) {
            unsigned long idx = (i * exp->inner) + j;
            statistics_add_sample(exp->data, (double) idx);
        }
        // Signaler le travailleur suivant
	sem_post(exp->lock + ((exp->rank + 1)%exp->nr_thread)*sizeof(sem_t));
    }
    return NULL;
}

void semrelay_init(struct experiment *exp) {
    int i;

    exp->data = make_statistics();
    // Allocation d'un tableau de sémaphores sem_t dans exp->lock
    exp->lock = calloc(exp->nr_thread, sizeof(sem_t));
    // Initialisation des sémaphores
    sem_t* lock = exp->lock;
    for(i = 0; i < exp->nr_thread; ++i){
        // On initialise la valeur de tous les semaphores a 0
        sem_init(lock+i*sizeof(sem_t), 0, 0);
    }
    // Le premier semaphore prend la valeur 1
    sem_post(lock);
}

void semrelay_done(struct experiment *exp) {
    int i;

    // copie finale dans exp->stats
    statistics_copy(exp->stats, exp->data);
    free(exp->data);

    // Destruction du verrou
    sem_t* lock = exp->lock;
    for(i = 0; i < exp->nr_thread; ++i)
        sem_destroy(lock+i*sizeof(sem_t));
    // Liberation de la memoire du verrou
    free(exp->lock);
    exp->lock = NULL;
}

